import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

@Injectable({
    providedIn: 'root'
})

export class HttpRequestService {

    apiURL = 'https://texttospeech.googleapis.com';
    key = 'AIzaSyAGYKqABUQuQqWmVpcSmAYLIskq5We4eDY';
    constructor(private http: HttpClient) { }

    httpOptions = {
        headers: new HttpHeaders({
            'Content-Type': 'application/json'
        })
    }
    postBar(data: any): Promise<any> {
    return this.http.post(this.apiURL + '/v1/text:synthesize?key=' + this.key, data).toPromise().then((
        response) => response).catch(error => console.log(error));
    }
}