import { Component, OnInit} from '@angular/core';
import { HttpRequestService } from '../services/http-request.service';
@Component({
  selector: 'app-bar',
  templateUrl: './bar.component.html',
  styleUrls: ['./bar.component.scss'],
})
export class BarComponent implements OnInit {
  constructor(private httpRequestService: HttpRequestService) {}
  text = '';
  minRate = '0.50';
  maxRate = '2.00';
  stepRate = 0.25;
  Rate = '1.00';
  minPitch = '-20.00';
  maxPitch = '20';
  stepPitch = 0.4;
  Pitch = '0.00';
  language = [
    { langId: 'th-TH', langName: 'ภาษาไทย' },
    { langId: 'en-US', langName: 'English' },
    { langId: 'fr-FR', langName: 'French' },
  ];
  x;
  y;
  languageSelected = null;
  voiceName: string = null;
  encode64;
  filename = 'file audio';
  encode = new Audio();
  loading: boolean;
  ngOnInit(): void {
    this.encode.onended = () => {
        this.loading = false;
    }
  }

  public async speech() {
    if (this.languageSelected != null) {
      const data = {
        input: {
          text: this.text,
        },
        voice: {
          languageCode: this.languageSelected,
          name: this.voiceName,
        },
        audioConfig: {
          audioEncoding: 'LINEAR16',
          speakingRate: this.Rate,
          pitch: this.Pitch,
        },
      };
      const response = await this.httpRequestService.postBar(data);
      console.log('voice name :' + this.voiceName);
      const base64 = 'data:audio/ogg;base64,' + response.audioContent;
      this.encode64 = base64;

      this.encode.src = base64;
      this.encode.load();
      this.encode.play();
      this.loading = true;
    } else {
      alert('Plese! Select Languages');
    }
    console.log(this);
    // console.log(this.encode64);
  }
 public download() {
    const filename = 'text-to-speech.mp3';
    const elem = document.createElement('a');
    elem.setAttribute('href', this.encode64);
    elem.setAttribute('download', filename);
    elem.style.display = 'none';
    document.body.appendChild(elem);
    elem.click();
    document.body.removeChild(elem);
  }

  public getSelection() {
    if (window.getSelection().toString()) {
      if (this.text !== window.getSelection().toString()) {
        this.text = window.getSelection().toString();
        console.log(this.text);
        const bar = document.getElementById('bar');
        bar.style.visibility = 'visible';
        bar.style.left = this.x - 151 + 'px';
        bar.style.top = this.y + 30 + 'px';
      }
    }
  }

  public positionBar(e) {
    const x = e.clientX;
    const y = e.clientY;
    this.x = x;
    this.y = y;
  }
  public hideBar() {
    document.getElementById('bar').style.visibility = 'hidden';
  }

  checkVoicesSelect(event) {
    console.log(event)
    this.voiceName = null;
  }
}
